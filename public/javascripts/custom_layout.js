// Set timeout variables.
var timoutNow = 1800000; // Timeout in 30 mins.
var logoutUrl = "/logout" // URL to logout page.

var timeoutTimer;
// Start timers.
function StartTimers() {
    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
}

// Reset timers.
function ResetTimers() {
    clearTimeout(timeoutTimer);
    StartTimers();
}

// Logout the user.
function IdleTimeout() {
    window.location = logoutUrl;
}


Metronic.init(); // init metronic core componets
Layout.init(); // init layout
Index.init();
Tasks.initDashboardWidget();



//on load elements

jQuery(document).ready(function () {
   
//stop loader
//    document.getElementById("preLoader").style.display = "none";

    var d = new Date();
    var n = d.getFullYear();
    $("#year").html(n);

      //            document.getElementById("preLoader").style.width = "0%";
    // $("meta[name='timezone_custom']").attr("content", moment.tz.guess());
    // $.cookie("time_zone_name", moment.tz.guess());

    //ajax setup
    $.ajaxSetup({
        beforeSend: function () {
            document.getElementById("preLoader").style.display = "block";
        },
        error: function (xhr, status, error) {
            alertify.set('notifier', 'position', 'top-right');
            if (xhr.responseJSON) {

                alertify.error("Error : " + xhr.responseJSON.error);
            } else {
                console.log(xhr);
                alertify.error("An error occured: " + xhr.status + " " + xhr.statusText);
                // alertify.error(xhr.statusText);
            }
            //                            
        }
        ,
        complete: function (xhr, stat) {
            document.getElementById("preLoader").style.display = "none";
        }
        //    timeout: 30000 // sets timeout to 3 seconds
    });

    //*****get browser time zone*************8/
    $("meta[name='timezone_custom']").attr("content", moment.tz.guess());
    //fade out the alert
    $(".alert").fadeOut(10000);
    $.cookie("time_zone_name", moment.tz.guess());

});


        